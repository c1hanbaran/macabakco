class AddChannelIdToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :channel_id, :integer
  end
end
