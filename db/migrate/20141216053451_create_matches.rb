class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :league_id
      t.string :home
      t.string :away
      t.datetime :date_time
      t.string :channel

      t.timestamps
    end
    add_index :matches, :league_id
    
  end
end
