class AddFlagToLeagues < ActiveRecord::Migration
  def up
    add_column :leagues, :flag, :string
  end

  def down
    remove_column :leagues, :flag, :string
  end
end
