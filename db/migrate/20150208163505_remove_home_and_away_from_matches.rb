class RemoveHomeAndAwayFromMatches < ActiveRecord::Migration
  def change
    remove_column :matches, :home, :string
    remove_column :matches, :away, :string
  end
end
