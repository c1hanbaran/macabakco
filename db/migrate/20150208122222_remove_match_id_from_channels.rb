class RemoveMatchIdFromChannels < ActiveRecord::Migration
  def change
    remove_column :channels, :match_id, :integer
  end
end
