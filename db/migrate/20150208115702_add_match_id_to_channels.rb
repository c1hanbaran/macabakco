class AddMatchIdToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :match_id, :integer
  end
end
