class RemoveChannelFromMatches < ActiveRecord::Migration
  def up
    remove_column :matches, :channel, :string
  end

  def down
    add_column :matches, :channel, :string
  end
end
