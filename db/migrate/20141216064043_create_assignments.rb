class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.integer :league_id
      t.integer :team_id

      t.timestamps
    end
    add_index :assignments, :league_id
    add_index :assignments, :team_id
  end
end
