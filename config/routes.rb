Macabakco::Application.routes.draw do
  
  root 'leagues#index'

  devise_for :admins

  get 'about', to: 'homes#about'
  get 'advertisement', to: 'homes#advertisement'
  get 'contact', to: 'homes#contact'
  resources :leagues, only: [:index] do           
      resources :matches, only: [:index]
  end  

  namespace :admin do
    get 'home', to: 'homes#home_action'
    get 'about', to: 'homes#about'
    get 'advertisement', to: 'homes#advertisement'
    get 'contact', to: 'homes#contact'
    get 'teamsbyleague', to: 'teams#teamsbyleague' #categorize teams by league
    resources :channels
    resources :leagues
    resources :teams
    resources :matches, only: [:destroy]
    resources :leagues  do
      resources :teams
      resources :matches do
        collection do
          get :bulk_edit
          put :bulk_update
        end 
      end
    end  
  end

  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
