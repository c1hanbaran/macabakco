class League < ActiveRecord::Base
	has_many :assignments
	has_many :matches
	has_many :teams, through: :assignments
	validates_presence_of :name
  	validates_uniqueness_of :name
	
end
