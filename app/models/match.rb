class Match < ActiveRecord::Base
	
	validates_uniqueness_of :home_id, scope: [:away_id, :league_id]
	validates_presence_of :date_time
	validates_with DifferentTeam


	belongs_to :league
	belongs_to :channel	
	belongs_to :away_team, class_name: "Team", foreign_key: :away_id
    belongs_to :home_team, class_name: "Team", foreign_key: :home_id


 end
