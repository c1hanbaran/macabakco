class DifferentTeam < ActiveModel::Validator
  def validate(record)
    if record.home_id == record.away_id
      record.errors[:home_id] << '"home_id and away_id cannot be the same!"'
    end
  end
end
