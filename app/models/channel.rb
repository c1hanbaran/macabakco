class Channel < ActiveRecord::Base
	has_many :matches, dependent: :destroy
	validates_presence_of :name
	validates_uniqueness_of :name
end
