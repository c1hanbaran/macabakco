class Team < ActiveRecord::Base
	has_many :assignments
	has_many :leagues, through: :assignments
	has_many :home_matches, class_name: "Match", foreign_key: :home_id, dependent: :destroy
  	has_many :away_matches, class_name: "Match", foreign_key: :away_id, dependent: :destroy
  	validates_presence_of :leagues
  	validates_presence_of :name
  	validates_uniqueness_of :name
end
