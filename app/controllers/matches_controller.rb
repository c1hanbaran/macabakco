class MatchesController < ApplicationController

	before_action :set_matches, only: [:index]

	def index
		#set_matches runs.
	end

	private
  # Use callbacks to share common setup or constraints between actions.
  def set_matches
    @league = League.find(params[:league_id])
    @matches = @league.matches
  end

end
