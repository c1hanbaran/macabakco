class Admin::MatchesController < ApplicationController

	before_action :set_matches, only: [:index, :new, :create, :update, :bulk_edit, :bulk_update]
  before_action :authenticate_admin!
  	
  

	def index
	 #set_matches runs!
	end

  def new
    @match = Match.new
    @channels = Channel.all
    @teams = Team.all 
  end

  def create

    error = false
    @match = @league.matches.new(match_params)

    respond_to do |format|
        if !@match.save
          format.html { redirect_to new_admin_league_match_path(@league), notice: "Match coudnt created! :( #{@match.errors.full_messages.join(",")}"  }
        else
          format.html { redirect_to admin_league_matches_path(@match.league), notice: 'Matches was successfully created! :)' }
        end
    end

  end

  def bulk_edit
    #set_matches runs.
    @teams = @league.teams
    @channels = Channel.all
    @leagues = League.all 
  end




  def bulk_update
    error = false
    matches = params[:match]
    last_match = nil
    matches.each do |id, data|
      m = Match.find(id)
      m.home_id = data["home_team"]
      m.away_id = data["away_team"]
      m.channel_id = data["channel"]
      m.date_time = DateTime.parse(data["date_time"])
      if !m.save
        last_match = m
        error = true
        break
      end
    end

    respond_to do |format|
        if error
          format.html { redirect_to bulk_edit_admin_league_matches_path(@league), 
            notice: "Matches coudnt updated! Maybe home and away
             teams are the same in a match or you have tried 
             to save the same match two times! :( #{last_match.errors.full_messages.join(",")}" }
        else
          format.html { redirect_to admin_league_matches_path(@league), notice: 'Matches were successfully updated! :)' }
        end
      end
  end

  def destroy
    @match = Match.find(params[:id])
    @league = @match.league
    @match.destroy

    respond_to do |format|
      format.html { redirect_to admin_league_matches_path(@league), notice: 'Match was successfully deleted.' }
    end
  end

 #######################Private Methods###########################################
	private
  # Use callbacks to share common setup or constraints between actions.

  def set_matches
    @league = League.find(params[:league_id])
    @matches = @league.matches
  end

    # Never trust parameters from the scary internet, only allow the white list through.
  def match_params
    params.require(:match).permit(:home_id, :away_id, :channel_id, :date_time)
  end
 ################################################################################


end