class Admin::LeaguesController < ApplicationController

   before_action :set_league, only: [:show, :edit, :update, :destroy]	
   before_action :authenticate_admin!

   def index
		@leagues = League.all
   end

   def show
   		#Set league runs.
   end

   def new
    @league = League.new
  end

   def edit
   		#Set league runs.
   end

   def create
		@league = League.new(league_params)

		respond_to do |format|
	      if @league.save
	        format.html { redirect_to admin_league_path(@league), notice: 'League was successfully created.' }
	      else
	        format.html { render action: 'new', notice: "League coudnt created! :( #{@league.errors.full_messages.join(",")}" }
	      end
    	end
   end

   def update
   	#Set league runs.
    respond_to do |format|
      if @league.update(league_params)
        format.html { redirect_to admin_leagues_path, notice: 'League was successfully updated.' }
      else
        format.html { render action: 'edit', notice: "League coudnt updated! :( #{@league.errors.full_messages.join(",")}" }
      end
    end
  end

  def destroy
  	#Set league runs.
    @league.destroy
    respond_to do |format|
      format.html { redirect_to admin_leagues_path, notice: 'League was successfully deleted.' }
    end
  end



  private
  
  # Use callbacks to share common setup or constraints between actions.
    def set_league
      @league = League.find(params[:id])
    end	  
  # Never trust parameters from the scary internet, only allow the white list through.
  def league_params
      params.require(:league).permit(:id,:name, :flag)
  end
end 