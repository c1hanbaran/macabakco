class Admin::ChannelsController < ApplicationController

   before_action :set_channel, only: [:show, :edit, :update, :destroy]	
   before_action :authenticate_admin!

   def index
		@channels = Channel.all
   end

   def show
   		#Set channel runs.
   end

   def new
    @channel = Channel.new
  end

   def edit
   		#Set channel runs.
   end

   def create
		@channel = Channel.new(channel_params)

		respond_to do |format|
	      if @channel.save
	        format.html { redirect_to admin_channel_path(@channel), notice: 'Channel was successfully created.' }
	      else
	        format.html { render action: 'new', notice: "Channel coudnt created! :( #{@channel.errors.full_messages.join(",")}" }
	      end
    	end
   end

   def update
   	#Set channel runs.
    respond_to do |format|
      if @channel.update(channel_params)
        format.html { redirect_to admin_channels_path, notice: 'Channel was successfully updated.' }
      else
        format.html { render action: 'edit', notice: "Channel coudnt updated! :( #{@channel.errors.full_messages.join(",")}" }
      end
    end
  end

  def destroy
  	#Set channel runs.
    @channel.destroy
    respond_to do |format|
      format.html { redirect_to admin_channels_path, notice: 'Channel was successfully deleted.' }
    end
  end



  private
  
  # Use callbacks to share common setup or constraints between actions.
    def set_channel
      @channel = Channel.find(params[:id])
    end	  
  # Never trust parameters from the scary internet, only allow the white list through.
  def channel_params
      params.require(:channel).permit(:id,:name)
  end
end 