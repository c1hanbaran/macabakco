class Admin::TeamsController < ApplicationController

   before_action :set_team, only: [:show, :destroy]	
   before_action :authenticate_admin!


   def index
    @league = League.find(params[:league_id])
    @@deneme = @league.id
    @teams = @league.teams
   end

   def show
   		#set_team runs.
   end

   def teamsbyleague
    @leagues = League.all
   end

   def new
    @leagues = League.all
    @team = Team.new
   end


   def create
    @leagues = League.all
		@team = Team.new(team_params.except(:leagues))

    team_params[:leagues].each do |league| 
    @team.leagues << League.find(league) if league != ""
    end

		respond_to do |format|
	      if @team.save
	        format.html { redirect_to admin_teamsbyleague_path, notice: 'Team was successfully created.' }
	      else
	        format.html { render action: 'new', notice: '#{@team.errors.full_messages.join(",")}' }
	      end
    	end
   end



  def destroy
  	#Set team runs.
    @team.destroy
    respond_to do |format|
      format.html { redirect_to admin_teamsbyleague_path, notice: 'Team was successfully deleted.' }
    end
  end



  private
  
  # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end	  
  # Never trust parameters from the scary internet, only allow the white list through.
  def team_params
      params.require(:team).permit(:id, :name, leagues:[])
  end
end 